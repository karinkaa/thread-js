const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  FORGOT: '/forgot',
  RESET: '/reset/:token',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
