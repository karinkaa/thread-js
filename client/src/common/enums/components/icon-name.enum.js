const IconName = {
  AT: 'at',
  COMMENT: 'comment',
  COPY: 'copy',
  DELETE: 'delete',
  EDIT: 'edit',
  FROWN: 'frown',
  IMAGE: 'image',
  LOCK: 'lock',
  LOG_OUT: 'log out',
  SHARE_ALTERNATE: 'share alternate',
  SPINNER: 'spinner',
  STATUS: 'status',
  THUMBS_UP: 'thumbs up',
  THUMBS_DOWN: 'thumbs down',
  USER: 'user',
  USER_CIRCLE: 'user circle'
};

export { IconName };
