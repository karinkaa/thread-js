const NotificationMessage = {
  LIKED_POST: 'Your post was liked!',
  DISLIKED_POST: 'Your post was disliked!',
  FORGOT_PASSWORD: 'Check your email!',
  RESET_PASSWORD: 'Your password was updated!'
};

export { NotificationMessage };
