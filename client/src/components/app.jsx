import { AppRoute, StorageKey } from 'common/enums/enums';
import {
  Header, Notifications, PrivateRoute,
  PublicRoute, Spinner
} from 'components/common/common';
import ForgotPassword from 'components/forgot-password/forgot-password';
import ResetPassword from 'components/forgot-password/reset-password';
import NotFoundPage from 'components/not-found/not-found';
import ProfilePage from 'components/profile/profile';
import SharedPostPage from 'components/shared-post/shared-post';
import SignPage from 'components/sign/sign';
import ThreadPage from 'components/thread/thread';
import { useCallback, useDispatch, useEffect, useSelector } from 'hooks/hooks';
import { Route, Routes } from 'react-router-dom';
import { storage } from 'services/services';
import { profileActionCreator, threadActionCreator } from 'store/actions';

const Routing = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();

  const hasToken = Boolean(storage.getItem(StorageKey.TOKEN));
  const hasUser = Boolean(user);

  const handlePostApply = useCallback(id => (
    dispatch(threadActionCreator.applyPost(id))
  ), [dispatch]);

  const handleUserLogout = useCallback(() => (
    dispatch(profileActionCreator.logout())
  ), [dispatch]);

  const handleUpdateProfile = useCallback(payload => (
    dispatch(profileActionCreator.updateProfile(payload))
  ), [dispatch]);

  useEffect(() => {
    if (hasToken) {
      dispatch(profileActionCreator.loadCurrentUser());
    }
  }, [hasToken, dispatch]);

  if (!hasUser && hasToken) {
    return <Spinner isOverflow />;
  }

  return (
    <div className="fill">
      {hasUser && (
        <header>
          <Header user={user} onUserLogout={handleUserLogout} handleUpdateProfile={handleUpdateProfile} />
        </header>
      )}
      <main className="fill">
        <Routes>
          <Route path={AppRoute.LOGIN} element={<PublicRoute component={SignPage} />} />
          <Route path={AppRoute.REGISTRATION} element={<PublicRoute component={SignPage} />} />
          <Route path={AppRoute.ROOT} element={<PrivateRoute component={ThreadPage} />} />
          <Route path={AppRoute.PROFILE} element={<PrivateRoute component={ProfilePage} />} />
          <Route path={AppRoute.FORGOT} element={<PublicRoute component={ForgotPassword} />} />
          <Route path={AppRoute.RESET} element={<PublicRoute component={ResetPassword} />} />
          <Route path={AppRoute.SHARE_$POSTHASH} element={<PrivateRoute component={SharedPostPage} />} />
          <Route path={AppRoute.ANY} element={<NotFoundPage />} />
        </Routes>
      </main>
      <Notifications onPostApply={handlePostApply} user={user} />
    </div>
  );
};

export default Routing;
