import { IconName } from 'common/enums/enums';
import PropTypes from 'prop-types';
import Icon from '../icon/icon';
import styles from './styles.module.scss';

const IconButton = ({
  iconName,
  label,
  onClick,
  onMouseOver
}) => (
  <button
    className={styles.iconButton}
    type="button"
    onClick={onClick}
    onMouseOver={onMouseOver}
    onFocus={onMouseOver}
  >
    <Icon name={iconName} />
    {label}
  </button>
);

IconButton.propTypes = {
  iconName: PropTypes.oneOf(Object.values(IconName)).isRequired,
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClick: PropTypes.func.isRequired,
  onMouseOver: PropTypes.func
};

IconButton.defaultProps = {
  label: '',
  onMouseOver: undefined
};

export default IconButton;
