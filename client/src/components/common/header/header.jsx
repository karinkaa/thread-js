import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { AppRoute, ButtonType, IconName, IconSize } from 'common/enums/enums';
import { userType } from 'common/prop-types/prop-types';
import { Button, Icon, Image, Input, NavLink } from 'components/common/common';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import styles from './styles.module.scss';

const Header = ({ user, onUserLogout, handleUpdateProfile }) => {
  const [newStatus, setNewStatus] = useState(user.status ?? '');
  const handleUpdateUserStatus = e => setNewStatus(e.target.value);

  const handleSaveNewStatus = e => {
    if (e.keyCode === 13) {
      e.target.blur();
      handleUpdateUserStatus(e);
      handleUpdateProfile({ ...user, status: newStatus });
    }
  };

  useEffect(() => {
    setNewStatus(user.status ?? '');
  }, [user.status]);

  return (
    <div className={styles.headerWrp}>
      {user && (
        <NavLink to={AppRoute.ROOT}>
          <div className={styles.userWrapper}>
            <Image
              isCircular
              width="45"
              height="45"
              src={user.image?.link ?? DEFAULT_USER_AVATAR}
              alt="user avatar"
            />
            {' '}
            <div className={styles.divStatus}>
              {user.username}
              {user.status && (
                <Input
                  id="status"
                  className={styles.status}
                  placeholder="Status"
                  value={newStatus}
                  onChange={handleUpdateUserStatus}
                  onKeyDown={handleSaveNewStatus}
                />
              )}
            </div>
          </div>
        </NavLink>
      )}
      <div>
        <NavLink to={AppRoute.PROFILE} className={styles.menuBtn}>
          <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
        </NavLink>
        <Button
          className={`${styles.menuBtn} ${styles.logoutBtn}`}
          onClick={onUserLogout}
          type={ButtonType.BUTTON}
          iconName={IconName.LOG_OUT}
          iconSize={IconSize.LARGE}
          isBasic
        />
      </div>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  handleUpdateProfile: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
