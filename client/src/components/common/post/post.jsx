import { IconName } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { IconButton, Image } from 'components/common/common';
import { getFromNowTime } from 'helpers/helpers';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useState } from '../../../hooks/hooks';
import EditPost from '../../thread/components/edit-post/edit-post';
import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  handlePostUpdate,
  handlePostDelete,
  handleGetPostReactionByPostId
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);
  const { userId, authors } = useSelector(state => ({
    userId: state.profile.user.id,
    authors: state.posts.postReactionAuthors
  }));
  const [open, setOpen] = useState(false);
  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditPostToggle = () => setOpen(!open);

  return (
    <div className={styles.card}>
      {image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>{`posted by ${user.username} - ${date}`}</span>
        </div>
        <p className={styles.description}>{body}</p>
      </div>
      <div className={styles.extra}>
        <ul>
          <li>
            <IconButton
              iconName={IconName.THUMBS_UP}
              label={likeCount}
              onClick={handlePostLike}
              onMouseOver={() => handleGetPostReactionByPostId({ id, isLike: true })}
            />
            {authors.length ? (
              <ul>
                {authors.map(author => <li key={author}>{author}</li>)}
              </ul>
            ) : null}
          </li>
          <li>
            <IconButton
              iconName={IconName.THUMBS_DOWN}
              label={dislikeCount}
              onClick={handlePostDislike}
              onMouseOver={() => handleGetPostReactionByPostId({ id, isLike: false })}
            />
            {authors.length ? (
              <ul>
                {authors.map(author => <li key={author}>{author}</li>)}
              </ul>
            ) : null}
          </li>
        </ul>
        <IconButton
          iconName={IconName.COMMENT}
          label={commentCount}
          onClick={handleExpandedPostToggle}
        />
        <IconButton
          iconName={IconName.SHARE_ALTERNATE}
          onClick={() => sharePost(id)}
        />
        {user && user.id === userId && (
          <div className={styles.buttons}>
            <IconButton
              iconName={IconName.EDIT}
              onClick={handleEditPostToggle}
            />
            <IconButton
              iconName={IconName.DELETE}
              onClick={() => handlePostDelete(post)}
            />
          </div>
        )}
        {open && <EditPost post={post} onClose={handleEditPostToggle} handlePostUpdate={handlePostUpdate} />}
      </div>
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  handlePostUpdate: PropTypes.func.isRequired,
  handlePostDelete: PropTypes.func.isRequired,
  handleGetPostReactionByPostId: PropTypes.func.isRequired
};

export default Post;
