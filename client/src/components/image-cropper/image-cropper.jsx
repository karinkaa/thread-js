import { Button } from 'components/common/common';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

function generateDownload(canvas, crop, onChangeImage) {
  if (!crop || !canvas) {
    return;
  }

  canvas.toBlob(blob => {
    onChangeImage(blob);
  });
}

const ImageCropper = ({ onChangeImage }) => {
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ unit: 'px', width: 200, height: 200 });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const ccrop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = ccrop.width * pixelRatio * scaleX;
    canvas.height = ccrop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      ccrop.x * scaleX,
      ccrop.y * scaleY,
      ccrop.width * scaleX,
      ccrop.height * scaleY,
      0,
      0,
      ccrop.width * scaleX,
      ccrop.height * scaleY
    );
  }, [completedCrop]);

  return (
    <div className="App">
      <div>
        <input type="file" accept="image/*" onChange={onSelectFile} />
      </div>
      <ReactCrop
        maxWidth={200}
        maxHeight={200}
        imageStyle={{ maxHeight: '500px' }}
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => setCrop(c)}
        onComplete={c => setCompletedCrop(c)}
      />
      <canvas
        ref={previewCanvasRef}
        style={{
          width: Math.round(completedCrop?.width ?? 0),
          height: Math.round(completedCrop?.height ?? 0),
          display: 'none'
        }}
      />
      <div>
        {completedCrop && (
          <Button
            color="teal"
            isDisabled={!completedCrop?.width || !completedCrop?.height}
            onClick={() => generateDownload(previewCanvasRef.current, completedCrop, onChangeImage)}
          >
            Set cropped image as avatar
          </Button>
        )}
      </div>
    </div>
  );
};

ImageCropper.propTypes = {
  onChangeImage: PropTypes.func.isRequired
};

export default ImageCropper;
