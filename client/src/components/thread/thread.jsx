import { Checkbox, Post, Spinner } from 'components/common/common';
import {
  useCallback, useDispatch, useEffect, useSelector, useState
} from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { image as imageService } from 'services/services';
import { threadActionCreator } from 'store/actions';
import { AddPost, ExpandedPost, SharedPostLink } from './components/components';
import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  likesUserId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showPostsLikedByMe, setShowPostsLikedByMe] = useState(false);

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostUpdate = useCallback(
    postPayload => dispatch(threadActionCreator.updatePost(postPayload)),
    [dispatch]
  );

  const handlePostDelete = useCallback(
    postPayload => dispatch(threadActionCreator.deletePost(postPayload)),
    [dispatch]
  );

  const handleGetPostReactionByPostId = useCallback(
    postPayload => dispatch(threadActionCreator.getPostReactionsByPostId(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowPostsLikedByMe = () => {
    setShowPostsLikedByMe(!showPostsLikedByMe);
    postsFilter.likesUserId = showPostsLikedByMe ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          isChecked={showOwnPosts}
          label="Show only my posts"
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          isChecked={showPostsLikedByMe}
          label="Show posts liked by me"
          onChange={toggleShowPostsLikedByMe}
        />
      </div>
      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            handlePostUpdate={handlePostUpdate}
            handlePostDelete={handlePostDelete}
            handleGetPostReactionByPostId={handleGetPostReactionByPostId}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          handlePostUpdate={handlePostUpdate}
          handlePostDelete={handlePostDelete}
          handleGetPostReactionByPostId={handleGetPostReactionByPostId}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
