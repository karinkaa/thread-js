import { getFromNowTime } from 'helpers/helpers';
import { postType } from 'common/prop-types/prop-types';
import { Button, Image, Modal, TextArea } from 'components/common/common';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import { useState } from '../../../../hooks/hooks';
import { ButtonColor } from '../../../../common/enums/components/button-color.enum';

const EditPost = ({ post, onClose, handlePostUpdate }) => {
  const {
    id,
    image,
    body,
    user,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);
  const [textValue, setTextValue] = useState(body);
  const handleChangeTextValue = e => setTextValue(e.target.value);

  return (
    <Modal
      isOpen
      onClose={onClose}
    >
      <div className={styles.card}>
        {image && <Image src={image.link} alt="post image" />}
        <div className={styles.content}>
          <div className={styles.meta}>
            <span>{`posted by ${user.username} - ${date}`}</span>
          </div>
          <TextArea value={textValue} placeholder="sdf" onChange={handleChangeTextValue} />
        </div>
      </div>
      <Button
        color={ButtonColor.BLUE}
        onClick={() => {
          handlePostUpdate({ id, body: textValue });
          onClose();
        }}
      >
        Update
      </Button>
    </Modal>
  );
};

EditPost.propTypes = {
  post: postType.isRequired,
  onClose: PropTypes.func.isRequired,
  handlePostUpdate: PropTypes.func.isRequired
};

export default EditPost;
