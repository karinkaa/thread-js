import { Modal, Post, Spinner } from 'components/common/common';
import { useCallback, useDispatch, useSelector } from 'hooks/hooks';
import PropTypes from 'prop-types';
import { threadActionCreator } from 'store/actions';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost, handlePostUpdate, handlePostDelete, handleGetPostReactionByPostId
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handlePostLike = useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleCommentAdd = useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  const deletePost = () => {
    handlePostDelete(post);
    handleExpandedPostClose();
  };

  return (
    <Modal
      isOpen
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            handlePostUpdate={handlePostUpdate}
            handlePostDelete={deletePost}
            handleGetPostReactionByPostId={handleGetPostReactionByPostId}
          />
          <div>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment key={comment.id} comment={comment} />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </div>
        </>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  handlePostUpdate: PropTypes.func.isRequired,
  handlePostDelete: PropTypes.func.isRequired,
  handleGetPostReactionByPostId: PropTypes.func.isRequired
};

export default ExpandedPost;
