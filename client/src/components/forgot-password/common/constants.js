import { UserPayloadKey } from 'common/enums/enums';

const DEFAULT_FORGOT_PASSWORD_PAYLOAD = {
  [UserPayloadKey.EMAIL]: ''
};

const DEFAULT_RESET_PASSWORD_PAYLOAD = {
  [UserPayloadKey.PASSWORD]: ''
};

export {
  DEFAULT_FORGOT_PASSWORD_PAYLOAD,
  DEFAULT_RESET_PASSWORD_PAYLOAD
};
