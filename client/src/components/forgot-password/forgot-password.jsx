import { ButtonColor, ButtonSize, ButtonType, IconName, NotificationMessage, UserPayloadKey } from 'common/enums/enums';
import { Button, FormInput, NavLink, Segment } from 'components/common/common';
import { useAppForm } from 'hooks/hooks';
import React, { useCallback, useState } from 'react';
import { NotificationManager } from 'react-notifications';
import { useDispatch } from 'react-redux';
import { profileActionCreator } from 'store/actions';
import { forgotPassword } from 'validation-schemas/forgot-password/forgot-password.validation-schema';
import { DEFAULT_FORGOT_PASSWORD_PAYLOAD } from './common/constants';
import styles from './styles.module.scss';

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_FORGOT_PASSWORD_PAYLOAD,
    validationSchema: forgotPassword
  });

  const handleForgotPassword = useCallback(
    payload => dispatch(profileActionCreator.forgotPassword(payload)),
    [dispatch]
  );

  const handleLogin = values => {
    setIsLoading(true);
    handleForgotPassword(values)
      .unwrap()
      .then(() => {
        NotificationManager.info(NotificationMessage.FORGOT_PASSWORD);
      })
      .catch(err => {
        NotificationManager.error(err.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className={styles.forgot}>
      <section className={styles.form}>
        <div className={styles.header}>
          <h2 className={styles.title}>Forgot your password?</h2>
          <span className={styles.subtitle}>
            Fill out your email address, and we’ll send you instructions to reset your password.
          </span>
        </div>
        <form name="forgotPassword" onSubmit={handleSubmit(handleLogin)}>
          <Segment>
            <fieldset className={styles.fieldset}>
              <FormInput
                name={UserPayloadKey.EMAIL}
                type="email"
                placeholder="Email"
                iconName={IconName.AT}
                control={control}
                errors={errors}
              />
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isLoading={isLoading}
                isFluid
                isPrimary
              >
                Send password reset email
              </Button>
              <NavLink to="/" className={styles.nav}>
                <Button color={ButtonColor.TEAL}>Cancel</Button>
              </NavLink>
            </fieldset>
          </Segment>
        </form>
      </section>
    </div>
  );
};

export default ForgotPassword;
