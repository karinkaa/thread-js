import { ButtonColor, ButtonSize, ButtonType, IconName, NotificationMessage, UserPayloadKey } from 'common/enums/enums';
import { Button, FormInput, NavLink, Segment } from 'components/common/common';
import { useAppForm, useParams } from 'hooks/hooks';
import React, { useCallback, useState } from 'react';
import { NotificationManager } from 'react-notifications';
import { useDispatch } from 'react-redux';
import { profileActionCreator } from 'store/actions';
import { resetPassword } from 'validation-schemas/reset-password/reset-password.validation-schema';
import { DEFAULT_RESET_PASSWORD_PAYLOAD } from './common/constants';
import styles from './styles.module.scss';

const ResetPassword = () => {
  const dispatch = useDispatch();
  const { token } = useParams();
  const [isLoading, setIsLoading] = useState(false);

  const { control, errors, handleSubmit } = useAppForm({
    defaultValues: DEFAULT_RESET_PASSWORD_PAYLOAD,
    validationSchema: resetPassword
  });

  const handleResetPassword = useCallback(
    payload => dispatch(profileActionCreator.resetPassword(payload)),
    [dispatch]
  );

  const handleLogin = values => {
    setIsLoading(true);
    handleResetPassword({ ...values, token })
      .unwrap()
      .then(() => {
        setTimeout(() => {
          window.location.href = '/';
        }, 1500);
        NotificationManager.info(NotificationMessage.RESET_PASSWORD);
      })
      .catch(err => {
        NotificationManager.error(err.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className={styles.forgot}>
      <section className={styles.form}>
        <div className={styles.header}>
          <h2 className={styles.title}>Choose a new password</h2>
          <span className={styles.subtitle}>
            Create a new password that is at least 3 characters long.
          </span>
        </div>
        <form name="resetPassword" onSubmit={handleSubmit(handleLogin)}>
          <Segment>
            <fieldset className={styles.fieldset}>
              <FormInput
                name={UserPayloadKey.PASSWORD}
                type="password"
                placeholder="Password"
                iconName={IconName.LOCK}
                control={control}
                errors={errors}
              />
              <Button
                type={ButtonType.SUBMIT}
                color={ButtonColor.TEAL}
                size={ButtonSize.LARGE}
                isLoading={isLoading}
                isFluid
                isPrimary
              >
                Save new password
              </Button>
              <NavLink to="/" className={styles.nav}>
                <Button color={ButtonColor.TEAL}>Cancel</Button>
              </NavLink>
            </fieldset>
          </Segment>
        </form>
      </section>
    </div>
  );
};

export default ResetPassword;
