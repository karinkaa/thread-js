import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { AppRoute, ButtonColor, IconName, ImageSize } from 'common/enums/enums';
import { Button, Image, Input, NavLink } from 'components/common/common';
import ImageCropper from 'components/image-cropper/image-cropper';
import { useCallback, useDispatch, useSelector, useState } from 'hooks/hooks';
import { image as imageService } from 'services/services';
import { profileActionCreator } from 'store/actions';
import styles from './styles.module.scss';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const dispatch = useDispatch();
  const [newName, setNewName] = useState(user.username);
  const [newImage, setNewImage] = useState(user.image);
  const [newStatus, setNewStatus] = useState(user.status ?? '');

  const handleUpdateUserName = e => setNewName(e.target.value);
  const handleUpdateUserStatus = e => setNewStatus(e.target.value);

  const handleUpdateProfile = useCallback(
    payload => dispatch(profileActionCreator.updateProfile(payload)),
    [dispatch]
  );

  const handleUploadFile = file => {
    imageService.uploadImage(file).then(({ id, link }) => setNewImage({ id, link }));
  };

  return (
    <div className={styles.profile}>
      <ImageCropper onChangeImage={e => handleUploadFile(e)} />

      <Image
        alt="profile avatar"
        isCentered
        src={newImage?.link ?? DEFAULT_USER_AVATAR}
        size={ImageSize.MEDIUM}
        isCircular
      />
      <Input
        iconName={IconName.USER}
        placeholder="Username"
        value={newName}
        onChange={handleUpdateUserName}
      />
      <Input
        iconName={IconName.AT}
        placeholder="Email"
        type="email"
        value={user.email}
        disabled
      />
      <Input
        iconName={IconName.STATUS}
        placeholder="Status"
        value={newStatus}
        onChange={handleUpdateUserStatus}
      />
      <NavLink to={AppRoute.ROOT} className={styles.nav}>
        <Button
          color={ButtonColor.BLUE}
          onClick={() => handleUpdateProfile({
            id: user.id,
            username: newName,
            image: newImage,
            status: newStatus
          })}
        >
          Save
        </Button>
      </NavLink>
    </div>
  );
};

export default Profile;
