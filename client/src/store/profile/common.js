const ActionType = {
  LOG_IN: 'profile/log-in',
  LOG_OUT: 'profile/log-out',
  REGISTER: 'profile/register',
  FORGOT: 'profile/forgot',
  RESET: 'profile/reset',
  UPDATE_PROFILE: 'profile/update'
};

export { ActionType };
