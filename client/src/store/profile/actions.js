import { createAsyncThunk } from '@reduxjs/toolkit';
import { ExceptionMessage, HttpCode, StorageKey } from 'common/enums/enums';
import { HttpError } from 'exceptions/exceptions';
import { ActionType } from './common';

const login = createAsyncThunk(
  ActionType.LOG_IN,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.login(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const register = createAsyncThunk(
  ActionType.REGISTER,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.registration(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const logout = createAsyncThunk(
  ActionType.LOG_OUT,
  (_request, { extra: { services } }) => {
    services.storage.removeItem(StorageKey.TOKEN);

    return null;
  }
);

const loadCurrentUser = createAsyncThunk(
  ActionType.LOG_IN,
  async (_request, { dispatch, rejectWithValue, extra: { services } }) => {
    try {
      return await services.auth.getCurrentUser();
    } catch (err) {
      const isHttpError = err instanceof HttpError;

      if (isHttpError && err.status === HttpCode.UNAUTHORIZED) {
        dispatch(logout());
      }

      return rejectWithValue(err?.message ?? ExceptionMessage.UNKNOWN_ERROR);
    }
  }
);

const updateProfile = createAsyncThunk(
  ActionType.UPDATE_PROFILE,
  async (request, { extra: { services } }) => {
    const { user } = await services.auth.updateProfile(request);
    return user;
  }
);

const forgotPassword = createAsyncThunk(
  ActionType.FORGOT,
  async (request, { extra: { services } }) => {
    const user = await services.auth.forgotPassword(request);
    return user;
  }
);

const resetPassword = createAsyncThunk(
  ActionType.RESET,
  async (request, { extra: { services } }) => {
    const user = await services.auth.resetPassword(request);
    return user;
  }
);

export { login, register, logout, loadCurrentUser, updateProfile, forgotPassword, resetPassword };
