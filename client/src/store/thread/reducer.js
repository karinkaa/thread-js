import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';

const initialState = {
  posts: [],
  postReactionAuthors: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.toggleExpandedPost.fulfilled, (state, action) => {
    const { post } = action.payload;

    state.expandedPost = post;
  });
  builder.addCase(threadActions.updatePost.fulfilled, (state, action) => {
    const { post } = action.payload;
    const index = state.posts.findIndex(item => item.id === post.id);
    if (state.expandedPost) state.expandedPost = post;
    state.posts.splice(index, 1, post);
  });
  builder.addCase(threadActions.deletePost.fulfilled, (state, action) => {
    const { post } = action.payload;
    const index = state.posts.findIndex(item => item.id === post.id);
    if (state.expandedPost) state.expandedPost = post;
    state.posts.splice(index, 1);
  });
  builder.addCase(threadActions.getPostReactionsByPostId.pending, state => {
    state.postReactionAuthors = [];
  });
  builder.addCase(threadActions.getPostReactionsByPostId.fulfilled, (state, action) => {
    const { authors } = action.payload;
    state.postReactionAuthors = authors;
  });
  builder.addMatcher(isAnyOf(threadActions.likePost.fulfilled, threadActions.addComment.fulfilled), (state, action) => {
    const { posts, expandedPost } = action.payload;
    state.posts = posts;
    state.expandedPost = expandedPost;
  });
  builder.addMatcher(isAnyOf(
    threadActions.applyPost.fulfilled,
    threadActions.createPost.fulfilled
  ), (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
});

export { reducer };
