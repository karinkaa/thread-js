import { ContentType, HttpMethod } from 'common/enums/enums';

class Auth {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  login(payload) {
    return this._http.load(`${this._apiPath}/auth/login`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load(`${this._apiPath}/auth/register`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load(`${this._apiPath}/auth/user`, {
      method: HttpMethod.GET
    });
  }

  updateProfile({ id, username, image, status }) {
    return this._http.load(`${this._apiPath}/auth/user/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ username, image, status })
    });
  }

  forgotPassword({ email }) {
    return this._http.load(`${this._apiPath}/auth/forgot`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ email })
    });
  }

  resetPassword({ password, token }) {
    return this._http.load(`${this._apiPath}/auth/reset`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ password, token })
    });
  }
}

export { Auth };
