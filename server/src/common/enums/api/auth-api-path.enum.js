const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  FORGOT: '/forgot',
  RESET: '/reset',
  $PROFILE: '/user/:id'
};

export { AuthApiPath };
