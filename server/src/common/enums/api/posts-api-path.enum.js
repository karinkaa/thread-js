const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  $REACT_ID: '/react/:id'
};

export { PostsApiPath };
