import { PostsApiPath } from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .get(PostsApiPath.$REACT_ID, req => postService.getPostReactionAuthors(req.params.id, req.query.isLike))
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit('new_post', post); // notify all users that a new post was created
      return post;
    })
    .put(PostsApiPath.$ID, async req => {
      const post = await postService.update(req.params.id, req.body);
      return post;
    })
    .put(PostsApiPath.REACT, async req => {
      const reaction = await postService.setReaction(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        if (reaction.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      return reaction;
    })
    .delete(PostsApiPath.$ID, req => postService.delete(req.params.id));

  done();
};

export { initPost };
