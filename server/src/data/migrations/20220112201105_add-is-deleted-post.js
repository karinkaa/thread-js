import { DbTableName } from '../../common/enums/enums';

export async function up(knex) {
  await knex.schema.alterTable(DbTableName.POSTS, table => {
    table
      .boolean('isDeleted').defaultTo(false);
  });
}

export async function down(knex) {
  await knex.schema.alterTable(DbTableName.POSTS, table => {
    table
      .dropColumn('isDeleted');
  });
}
