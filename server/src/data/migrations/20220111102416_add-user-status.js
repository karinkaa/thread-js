import { DbTableName } from '../../common/enums/enums';

export async function up(knex) {
  await knex.schema.alterTable(DbTableName.USERS, table => {
    table
      .string('status');
  });
}

export async function down(knex) {
  await knex.schema.alterTable(DbTableName.USERS, table => {
    table
      .dropColumn('status');
  });
}
