const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getWhereUserIdQuery = (model, userId, likesUserId) => builder => {
  if (userId) {
    builder.where({ userId });
  }
  if (likesUserId) {
    builder
      .whereExists(model.relatedQuery('postReactions')
        .where({ userId: likesUserId }));
  }
};

export { getCommentsCountQuery, getReactionsQuery, getWhereUserIdQuery };
