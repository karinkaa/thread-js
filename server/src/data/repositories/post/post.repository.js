import { Abstract } from '../abstract/abstract.repository';
import { getCommentsCountQuery, getReactionsQuery, getWhereUserIdQuery } from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  deleteById(id) {
    return this.updateById(id, { isDeleted: true });
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      likesUserId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereUserIdQuery(this.model, userId, likesUserId))
      .andWhere({ isDeleted: false })
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .andWhere({ isDeleted: false })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .first();
  }
}

export { Post };
