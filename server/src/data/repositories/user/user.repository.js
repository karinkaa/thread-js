import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor({ userModel }) {
    super(userModel);
  }

  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.query().select().where({ email }).first();
  }

  getByUsername(username) {
    return this.model.query().select().where({ username }).first();
  }

  getByToken(token) {
    return this.model.query().select().where({ resetPasswordToken: token }).first();
  }

  getUserById(id) {
    return this.model.query()
      .select('id', 'createdAt', 'email', 'updatedAt', 'username', 'status')
      .where({ id })
      .withGraphFetched('[image]')
      .first();
  }

  updateUser(id, data) {
    return this.updateById(id, data);
  }
}

export { User };
