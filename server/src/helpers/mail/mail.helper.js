import nodemailer from 'nodemailer';
import { ENV } from '../../common/enums/enums';

class Mail {
  constructor() {
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: ENV.MAIL.EMAIL_ADDRESS,
        pass: ENV.MAIL.EMAIL_PASSWORD
      }
    });
  }

  async sendResetPasswordMail(user) {
    return this.sendMail({
      from: 'bsa_2022_thread@gmail.com',
      to: `${user.email}`,
      subject: 'Link To Reset Password',
      text:
        'You are receiving this because you (or someone else) have requested '
        + 'the reset of the password for your account.\n\n'
        + 'Please click on the following link, or paste this into your browser '
        + 'to complete the process within one hour of receiving it:\n\n'
        + `${ENV.MAIL.CLIENT_URL}/reset/${user.resetPasswordToken}\n\n`
        + 'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    });
  }

  async sendMail(mailOptions) {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(mailOptions, (err, response) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    });
  }
}

const mail = new Mail();
export { mail };
