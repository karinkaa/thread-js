import { ExceptionMessage } from '../../common/enums/enums';
import { InvalidCredentialsError } from '../../exceptions/exceptions';
import {
  createToken,
  cryptCompare,
  encrypt,
  verifyToken
} from '../../helpers/helpers';
import { mail } from '../../helpers/mail/mail.helper';

class Auth {
  constructor({ userRepository }) {
    this._userRepository = userRepository;

    this.register = this.register.bind(this);
  }

  async verifyLoginCredentials({ email, password }) {
    const user = await this._userRepository.getByEmail(email);

    if (!user) {
      throw new InvalidCredentialsError(ExceptionMessage.INCORRECT_EMAIL);
    }

    const isEqualPassword = await cryptCompare(password, user.password);
    if (!isEqualPassword) {
      throw new InvalidCredentialsError(ExceptionMessage.PASSWORDS_NOT_MATCH);
    }

    return user;
  }

  async login({ id }) {
    return {
      token: createToken({ id }),
      user: await this._userRepository.getUserById(id)
    };
  }

  async register({ password, ...userData }) {
    const { email, username } = userData;

    const userByEmail = await this._userRepository.getByEmail(email);
    if (userByEmail) {
      throw new InvalidCredentialsError(ExceptionMessage.EMAIL_ALREADY_EXISTS);
    }

    const userByUsername = await this._userRepository.getByUsername(username);
    if (userByUsername) {
      throw new InvalidCredentialsError(
        ExceptionMessage.USERNAME_ALREADY_EXISTS
      );
    }

    const newUser = await this._userRepository.addUser({
      ...userData,
      password: await encrypt(password)
    });

    return this.login(newUser);
  }

  async verifyToken(token) {
    return verifyToken(token);
  }

  async updateProfile(id, { username, image, status }) {
    const userByUsername = await this._userRepository.getByUsername(username);

    if (userByUsername && String(userByUsername.id) !== String(id)) {
      throw new InvalidCredentialsError(
        ExceptionMessage.USERNAME_ALREADY_EXISTS
      );
    }

    const newUser = await this._userRepository.updateUser(id, { username, imageId: image.id, status });
    return this.login(newUser);
  }

  async generateResetToken({ email }) {
    const user = await this._userRepository.getByEmail(email);
    if (!user) {
      throw new InvalidCredentialsError(
        ExceptionMessage.INCORRECT_EMAIL
      );
    }

    const token = createToken({ id: user.id });
    const newUser = await this._userRepository.updateUser(
      user.id,
      {
        resetPasswordToken: token,
        resetPasswordExpires: new Date(Date.now() + 3600000).toISOString()
      }
    );

    return mail.sendResetPasswordMail(newUser);
  }

  async resetPassword({ token, password }) {
    const user = await this._userRepository.getByToken(token);
    if (!user) {
      throw new InvalidCredentialsError(
        ExceptionMessage.INVALID_TOKEN
      );
    }

    await this._userRepository.updateUser(
      user.id,
      {
        password: await encrypt(password),
        resetPasswordToken: null,
        resetPasswordExpires: null
      }
    );

    return { message: 'Password updated' };
  }
}

export { Auth };
